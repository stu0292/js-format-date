// formatDate returns a string from Date object d, formatted according to string fstr
// Format options start with a % sign
// Example: formatDate(new Date(), "%YY/%MM/%DD")
// Supported format options:
	// DD: Two digit day. eg 01
	// MM: Two digit month. eg 05
	// YYYY: Four digit year. eg 2018
	// YY: Two digit year. eg 18
let formatDate = function(d, fstr) {

	// pad("00", 5) -> 05
	// pad("00", 12) -> 12
	let pad = function(pad, subject) {
		str = "" + subject	// to string
		if (str.length >= pad.length) {
		    return str
		}
		return (pad + str).substr(str.length)
	}

	// pad with leading 0, length 2
	let padZ = function(subject) {
		return pad("00", subject)
	}

	// recognised special operators start with %
	a = fstr.split("%")

	// TODO: escape % char somehow (if % literal required in output string) 
	// Might be easiest just to loop through each char than faffing with regex

	// build the string based on each format option
	return a.reduce(function(result, option) {

		// Most options are 2 characters long
		// If an option is more than 2 chars, optionLen must be updated.
		// For example, option "YYYY" sets optionLen to 4.
		optionLen = 2

		// For each option, determine the part of the date to include,
		// join it to the result string 
		if (option.startsWith("DD")) {
			// %DD: Two digit day. eg 01
			result += padZ(d.getDate())
		}

		if (option.startsWith("MM")) {
			// %MM: Two digit month. eg 05
			result += padZ(d.getMonth() + 1)
		}

		if (option.startsWith("YYYY")) {
			// %YYYY: Four digit year. eg 2018
			optionLen = 4
			result += d.getFullYear()
		} else if (option.startsWith("YY")) {
			// %YY: Two digit year. eg 18 by converting to string and taking the last 2 chars
			result += (d.getFullYear().toString().substr(2,2))
		}

		// then include any trailing characters from the option string
		// FIXME optionLen is a fragile approach
		result += option.substring(optionLen)

		return result
	}, "")
}

module.exports = formatDate;
